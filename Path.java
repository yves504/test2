import java.util.ArrayList;
import java.util.List;

public class Path {
	private Double length = 110.0;
	private List<Integer> paths = new ArrayList<Integer>();

	public Double getLength() {
		return length;
	}

	public void setLength(Double length) {
		this.length = length;
	}

	public List<Integer> getPaths() {
		return paths;
	}

	public void setPaths(List<Integer> paths) {
		this.paths = paths;
	}
}